const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const dateFormat = require("dateformat");

//app.set("port", 8080);
app.set("port", 8080)
app.use(bodyParser.json({type:"application/json"}));
app.use(bodyParser.urlencoded({extended: true}));

const Pool = require("pg").Pool;
const config = {
  host: "localhost",
  user: "records",
  password: "Th3W0rmTurn5",
  database: "records"
};

const pool = new Pool(config);

app.get("/list-users", async(req, res) => {
  const type = req.query.type;
  try {
    if(type == "full"){
      const template = "SELECT * FROM users";
      const response = await pool.query(template, []);
      res.json({users: response.rows});
    }
    if(type == "summary") {
      const template = "SELECT firstname, lastname FROM users";
      const response = await pool.query(template, []);
      console.log();
      res.json({users: response.rows});
    }
  } catch (err) {
    console.log(err);
    res.json({error: err, status: 'Unforseen error occured, contact builder'});
  }
});

app.get("/list-workshops", async(req, res) => {
  try {
      const template = "SELECT title, class_date, location lastname FROM workshops";
      const response = await pool.query(template, []);
      res.json({users: response.rows});
  } catch (err) {
    console.log(err);
    res.json({error: err, status: 'Unforseen error occured, contact builder'});
  }
});

app.get("/attendees", async(req, res) => {
  const title = req.query.title;
  const date = req.query.date;
  const location = req.query.location;
  try {
    const attendeeRequest = "SELECT firstname, lastname FROM users JOIN enrolled ON users.username = enrolled.username WHERE title LIKE $1 AND class_date = $2::date AND location LIKE $3";
    const attendeeResponse = await pool.query(attendeeRequest, [title, date, location]);
    if (attendeeResponse.rowCount == 0) {
      const request = "SELECT title FROM workshops WHERE title LIKE $1 AND class_date = $2::date AND location LIKE $3"
      const response = await pool.query(request, [title, date, location]);
      if(response.rowCount == 0){
        res.json({error: "workshop does not exist"});
      } else {
        res.json({attendees: attendeeResponse.rows})
      }
    } else {
      res.json({attendees: attendeeResponse.rows});
    }
  } catch (err) {
    console.error("Error Running Post Request: " + err + " code: " + err.code);
    res.json({error: "unforseen error occured listing attendees, please contact builder"})
  }
});

app.post("/create-user", async(req, res) => {
  const firstname = req.body.firstname;
  const lastname = req.body.lastname;
  const username = req.body.username;
  const email = req.body.email;
  try {
    const template = "INSERT INTO users (firstname, lastname, username, email) VALUES ($1, $2, $3, $4)";
    const response = await pool.query(template, [firstname, lastname, username, email]);
    res.json({status: 'user added'});
  } catch (err) {
    console.error("Error Running Post Request: " + err + " code: " + err.code);
    if (err.code == 23502) {
      res.json({status: 'parameters not given'});
    } else if (err.code == 23505) {
      res.json({status: 'username taken'});
    } else {
      res.json({error: err, status: 'Unforseen error occured, contact builder'});
    }
  }
});

app.post("/add-workshop", async(req, res) => {
  const title = req.body.title;
  const date = req.body.date;
  const location = req.body.location;
  const maxseats = req.body.maxseats;
  const instructor = req.body.instructor;

  try {
    const template = "INSERT INTO workshops (title, class_date, location, maxseats, instructor) VALUES ($1, $2, $3, $4, $5)";
    const response = await pool.query(template, [title, date, location, maxseats, instructor]);
    res.json({status: 'workshop added'});
  } catch (err) {
    console.error("Error Running Post Request: " + err + " code: " + err.code);
    if (err.code == 23502) {
      res.json({error: 'parameters not given'});
    } else if (err.code == 23505) {
      res.json({status: 'workshop already in database'});
    } else {
      res.json({error: err, message: 'Unforseen error occured, contact builder'});
    }
  }
});

app.post("/enroll", async(req, res) => {
  const title = req.body.title;
  const date = req.body.date;
  const location = req.body.location;
  const username = req.body.username;
  try {
    const seatsRemainingRequest = "SELECT maxseats FROM workshops WHERE title LIKE $1 AND class_date = $2::date AND location LIKE $3"
    const seatsRemainingResponse = await pool.query(seatsRemainingRequest, [title, date, location]);
    if(seatsRemainingResponse.rowCount == 0){
      res.json({status: "workshop does not exist"});
    }
    else if(seatsRemainingResponse.rows[0].maxseats <= 0){
      res.json({status: "no seats available"});
    } else {
      try {
        const enrollmentRequest = "INSERT INTO enrolled (title, class_date, location, username) VALUES ($1, $2, $3, $4)";
        const enrollmentResponse = await pool.query(enrollmentRequest, [title, date, location, username]);
        const workshopRequest = "UPDATE workshops SET maxseats = maxseats-1 WHERE title LIKE $1 AND class_date = $2::date AND location LIKE $3"
        const workshopResponse = await pool.query(workshopRequest, [title, date, location]);
        res.json({status: "user added"});
      } catch (err) {
        console.error("Error Running Post Request: " + err + " code: " + err.code);
        if (err.code == 23505){
          res.json({status: "user already enrolled"});
        } else {
          res.json({status: "user not in database"});
        }
      }
    }
  } catch (err) {
    console.error("Error Running Post Request: " + err + " code: " + err.code);
    res.json({status: "unforseen error occured, please contact builder"});
  }
});

app.delete("/delete-user", async(req, res) => {
  const username = req.body.username;
  try {
    const template = "DELETE FROM users WHERE username LIKE $1";
    const response = await pool.query(template, [username]);
    res.json({status: "deleted"});
  } catch (err) {
    console.error("Error Running Post Request: " + err + " code: " + err.code);
    res.json({error: err, message: 'Unforseen error occured, contact builder'});
  }
});

app.listen(app.get("port"), () => {
  console.log(`Find the server at http://localhost:${app.get("port")}`);
});
