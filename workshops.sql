DROP DATABASE IF EXISTS records;
CREATE DATABASE records;
\c records

CREATE TABLE workshops
(
  title text NOT NULL,
  class_date date,
  location text,
  maxseats INTEGER,
  instructor text,
  UNIQUE(title, class_date, location)
);

CREATE TABLE enrolled
(
  title text NOT NULL,
  class_date date,
  location text,
  username text NOT NULL,
  UNIQUE(title, class_date, location, username)
);

CREATE TABLE users
(
  firstname text,
  lastname text,
  username text PRIMARY KEY,
  email text
);


GRANT UPDATE, SELECT, INSERT ON workshops to records;
/*GRANT USAGE on workshops_id_seq to records;*/
GRANT SELECT, INSERT ON enrolled to records;
/*GRANT USAGE on enrolled_id_seq to records;*/
GRANT SELECT, DELETE, INSERT ON users to records;
/*GRANT USAGE on users_id_seq to records;*/
